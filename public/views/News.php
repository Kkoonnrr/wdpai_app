<!DOCTYPE html>
<head>
<!--    <link rel="stylesheet" type="text/css" href="/public/css/style.css">-->
    <link rel="stylesheet" type="text/css" href="/public/css/projects.css">

    <script src="https://kit.fontawesome.com/723297a893.js" crossorigin="anonymous"></script>
    <meta http-equiv="Content-type" content="text/html; charset=iso-8859-2">
    <title>News</title>
</head>

<body>
    <div class="basecontainer">
        <main>
            <header>
                <form class="prof" action="profile" method="POST">
                    <div class="Profile"> <button>Profile</button></div>
                </form>
                <form class="frie" action="friends" method="POST">
                    <div class="Friends"> <button>Friends</button></div>
                </form>
                <form class="reco" action="records" method="POST">
                    <div class="Records"> <button>Records</button></div>
                </form>
                <form class="ne" action="projects" method="POST">
                    <div class="News"> <button class="news" type="submit">News</button></div>
                </form>
            </header>
            <section class="projects">
                <div id="project-1">
                    <div>
                        <h2>New Polish record in the 59 kg category!</h2>
                        <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim.</p>
                    </div>
                    <div class="social-section">
                            <i class="fas fa-heart"></i>97
                            <i class="fas fa-minus-square"></i> 35
                            <buttonSh>Share</buttonSh>
                    </div>
                </div>
                <div id="project-2">
                    <div>
                        <h2>Ranking of the strongest Poles!</h2>
                        <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim.</p>
                    </div>
                    <div class="social-section">
                        <i class="fas fa-heart"></i>303
                        <i class="fas fa-minus-square"></i> 342
                        <buttonSh>Share</buttonSh>
                    </div>
                </div>
                <div id="project-3">
                    <div>
                        <h2>John uploaded the movie and beat his life record!</h2>
                        <p></p>
                    </div>
                        <img src="/public/img/uploads/project_smile.jpg">
                    <div class="social-section">
                        <i class="fas fa-heart"></i>153
                        <i class="fas fa-minus-square"></i> 139
                        <buttonSh>Share</buttonSh>
                    </div>
                    </div>
                    <div id="project-4">
                        <div>
                            <h2>Competition calendar!</h2>
                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim.</p>
                        </div>
                        <div class="social-section">
                            <i class="fas fa-heart"></i>409
                            <i class="fas fa-minus-square"></i> 161
                            <buttonSh>Share</buttonSh>
                        </div>
                    </div>
                    <div id="project-5">
                        <div>
                            <h2>Whey or isolate?</h2>
                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim.</p>
                        </div>
                        <div class="social-section">
                            <i class="fas fa-heart"></i>296
                            <i class="fas fa-minus-square"></i> 121
                            <buttonSh>Share</buttonSh>
                        </div>
                    </div>
                    <div id="project-6">
                        <div>
                            <h2>Rosalia makes 5 new friends!</h2>
                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim.</p>
                        </div>
                        <div class="social-section">
                            <i class="fas fa-heart"></i>423
                            <i class="fas fa-minus-square"></i> 151
                            <buttonSh>Share</buttonSh>
                        </div>
                    </div>
</section>
        </main>
    </div>
</body>