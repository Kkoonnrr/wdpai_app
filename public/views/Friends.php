<!DOCTYPE html>
<head>
<!--    <link rel="stylesheet" type="text/css" href="public/css/style.css">-->
    <link rel="stylesheet" type="text/css" href="public/css/Friends.css">

    <script src="https://kit.fontawesome.com/723297a893.js" crossorigin="anonymous"></script>
    <title>Friends</title>
</head>

<body>
    <div class="basecontainer">
        <main>
            <header>
                <form class="prof" action="profile" method="POST">
                    <div class="Profile"> <button>Profile</button></div>
                </form>
                <form class="frie" action="friends" method="POST">
                    <div class="Friends"> <button class="friends" type="submit">Friends</button></div>
                </form>
                <form class="reco" action="records" method="POST">
                    <div class="Records"> <button>Records</button></div>
                </form>
                <form class="ne" action="projects" method="POST">
                    <div class="News"> <button>News</button></div>
                </form>
            </header>
            <section class="Friend">
                <h1>
                    <ul>
                        <li>
                            <div class="Image">
                                <img src="public/img/uploads/project_smile.jpg">
                            </div>
                            <div class="tekst">
                            <Gtext>Rosalia</Gtext>
                            <Htext>Total 200 kg</Htext>
                            </div>
                        </li>
                        <li>
                            <div class="Image">
                                <img src="public/img/uploads/project_smile.jpg">
                            </div>
                            <div class="tekst">
                                <Gtext>Charles</Gtext>
                                <Htext>Total 300 kg</Htext>
                            </div>

                        </li>
                        <li>
                            <div class="Image">
                                <img src="public/img/uploads/project_smile.jpg">
                            </div>
                            <div class="tekst">
                                <Gtext>John</Gtext>
                                <Htext>Total 400 kg</Htext>
                            </div>

                        </li>
                        <li>
                            <div class="Image">
                                <img src="public/img/uploads/project_smile.jpg">
                            </div>
                            <div class="tekst">
                                <Gtext>Joseph</Gtext>
                                <Htext>Total 430 kg</Htext>
                            </div>

                        </li>
                    </ul>
                    <buttonAF>Add new friend</buttonAF>
                </h1>
                <h2>
                    <div class="Image">
                        <img src="public/img/uploads/project_smile.jpg">
                    </div>
                    <form class="prof" action="profile" method="POST">
                        <div class="Profile"> <button class="show" type="submit">Show profile</button></div>
                    </form>
                    <ul class="menu">
                    <li>
                        Age<input name="email" type="text" placeholder="age">
                    </li>
                    <li>
                        Height<input name="password" type="text" placeholder="Height">
                    </li>
                    <li>
                        Weight<input name="password" type="text" placeholder="Weight">
                    </li>
                    <li>
                        Country<input name="password" type="text" placeholder="Country">
                    </li>
                </ul>
                </h2>
            </section>
        </main>
    </div>
</body>