<!DOCTYPE html>
<head>
<!--    <link rel="stylesheet" type="text/css" href="public/css/style.css">-->
    <link rel="stylesheet" type="text/css" href="public/css/Profile.css">

    <script src="https://kit.fontawesome.com/723297a893.js" crossorigin="anonymous"></script>
    <title>Profile</title>
</head>

<body>
    <div class="basecontainer">
        <main>
            <header>
                <form class="prof" action="profile" method="POST">
                <div class="Profile"> <button class="profile" type="submit">Profile</button></div>
                </form>
                <form class="frie" action="friends" method="POST">
                <div class="Friends"> <button>Friends</button></div>
                </form>
                <form class="reco" action="records" method="POST">
                <div class="Records"> <button>Records</button></div>
                </form>
                <form class="ne" action="projects" method="POST">
                <div class="News"> <button>News</button></div>
                </form>
            </header>
            <section class="Info">
                <h1>
                <ul class="menu">
                    <li>
                        Age<input name="email" type="text" placeholder="age">
                    </li>
                    <li>
                        Height<input name="password" type="text" placeholder="Height">
                    </li>
                    <li>
                        Weight<input name="password" type="text" placeholder="Weight">
                    </li>
                </ul>
                <div class="Image">
                    <img src="public/img/uploads/project_smile.jpg">
                </div>
                <ul class="menu">
                    <li>
                        Gender<input name="email" type="text" placeholder="Gender">
                    </li>
                    <li>
                        Country<input name="password" type="text" placeholder="Country">
                    </li>
                    <li>
                        Total<input name="password" type="text" placeholder="Total">
                    </li>
                </ul>
                </h1>
                <h2>
                    <ul class="Total">
                        <li>
                            Deadlift
                        </li>
                        <div class="Image">
                            <img src="public/img/uploads/project_smile.jpg">
                        </div>
                        <li>
                            <input name="password" type="text" placeholder="score">
                        </li>
                    </ul>
                    <ul class="Total">
                        <li>
                            Squat
                        </li>
                        <div class="Image">
                            <img src="public/img/uploads/project_smile.jpg">
                        </div>
                        <li>
                            <input name="password" type="text" placeholder="score">
                        </li>
                    </ul>
                    <ul class="Total">
                        <li>
                            Bench Press
                        </li>
                        <div class="Image">
                            <img src="public/img/uploads/project_smile.jpg">
                        </div>
                        <li>
                            <input name="password" type="text" placeholder="score">
                        </li>
                    </ul>
                </h2>
            </section>

        </main>
    </div>
</body>
