<!DOCTYPE html>
<head>
<!--    <link rel="stylesheet" type="text/css" href="/public/css/style.css">-->
    <link rel="stylesheet" type="text/css" href="/public/css/Records.css">

    <script src="https://kit.fontawesome.com/723297a893.js" crossorigin="anonymous"></script>
    <title>Records</title>
</head>

<body>
    <div class="basecontainer">
        <main>
            <header>
                <form class="prof" action="profile" method="POST">
                    <div class="Profile"> <button class="profile" type="submit">Profile</button></div>
                </form>
                <form class="frie" action="friends" method="POST">
                    <div class="Friends"> <button>Friends</button></div>
                </form>
                <form class="reco" action="records" method="POST">
                    <div class="Records"> <button class="records" type="submit">Records</button></div>
                </form>
                <form class="ne" action="projects" method="POST">
                    <div class="News"> <button>News</button></div>
                </form>
            </header>
        </main>
        <section class="Type">
            <ul class="menu">
                <div class="custom-select" style="width:200px;">
                    <select>
                        <option value="0">Select range:</option>
                        <option value="1">Poland</option>
                        <option value="2">World</option>
                    </select>
                </div>
                <div class="custom-select" style="width:200px;">
                    <select>
                        <option value="0">Select gender:</option>
                        <option value="1">Male</option>
                        <option value="2">Female</option>
                    </select>
                </div>
                <div class="custom-select" style="width:200px;">
                    <select>
                        <option value="0">Select weight category:</option>
                        <option value="1">59 kg</option>
                        <option value="2">66 kg</option>
                        <option value="3">74 kg</option>
                        <option value="4">83 kg</option>
                        <option value="5">93 kg</option>
                        <option value="6">105 kg</option>
                        <option value="7">120 kg</option>
                        <option value="8">120+ kg</option>
                    </select>
                </div>
                <div class="custom-select" style="width:200px;">
                    <select>
                        <option value="0">Select age:</option>
                        <option value="1">18</option>
                        <option value="2">23</option>
                        <option value="3">W I</option>
                        <option value="4">W II</option>
                        <option value="5">W II</option>
                        <option value="6">W IV</option>
                        <option value="7">Seniors</option>
                    </select>
                </div>
                <li>
                    <form class="prof" action="profile" method="POST">
                        <div class="Profile"> <button class="answer" type="submit"><i class="fas fa-arrow-right"></i></button></div>
                    </form>
                </li>
            </ul>

        </section>
    </div>
</body>