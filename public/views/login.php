<!DOCTYPE html>
<head>
    <title>LOGIN PAGE</title>
    <link rel="stylesheet" type="text/css" href="/public/css/style.css">
<script src="https://kit.fontawesome.com/723297a893.js" defer crossorigin="anonymous"></script>
</head>
<body>
<div class="container">
    <div class="logo">
        <img src="/public/img/logo.svg">
    </div>
    <div class="logincontainer">
        <form class="login" action="login" method="POST">
            <div class="messages">
                <?php if (isset($messages)){
                    foreach ($messages as $message) {
                        echo $message;
                    }
                }
                ?>
            </div>
            <ul class="menu">
                <li>
                    <i class="fas fa-envelope"></i>
                    <input name="email" type="text" placeholder="email@email.com">
                </li>
                <li>
                    <i class="fas fa-lock"></i>
                    <input name="password" type="password" placeholder="password">
                </li>
                <button type="submit">Login</button>
            </ul>
        </form>
    </div>
</div>
</body>