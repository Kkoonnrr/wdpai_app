<?php

require_once 'AppController.php';
require_once __DIR__.'/../models/User.php';

class SecurityController extends AppController
{
    public function login(){
        $user = new User('konrad@synowiec.eu', 'admin', 'kon', 'syn');

//        if($this->isPost()){
//            return $this->login('login');
//        }

        $email = $_POST["email"];
        $password = $_POST["password"];

        if ($user->getEmail() !== $email){
            return $this->render('login', ['messages' => ['User with this email not exist']]);
        }
        if ($user->getPassword() !== $password){
            return $this->render('login', ['messages' => ['Wrong password']]);
        }
        return $this->render('profile');
    }
    public function profile()
    {
        return $this->render('Profile');
    }
    public function projects()
    {
        return $this->render('News');
    }
    public function friends()
    {
        return $this->render('Friends');
    }
    public function records()
    {
        return $this->render('Records');
    }
}