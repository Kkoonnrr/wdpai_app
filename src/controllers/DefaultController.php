<?php

require_once 'AppController.php';

class DefaultController extends AppController{
	
	public function login() {
	    $this->render('login');
	}
	public function projects() {
        $this->render('News');
	}
    public function profile() {
        $this->render('Profile');
    }
    public function friends() {
        $this->render('Friends');
    }
    public function records() {
        $this->render('Records');
    }
}