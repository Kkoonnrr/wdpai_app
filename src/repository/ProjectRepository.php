<?php
//
//require_once 'Repository.php';
//require_once __DIR__.'/../models/User.php';
//
//class UserRepository extends Repository
//{
//
//    public function getUser(string $email): ?User
//    {
//        $stmt = $this->database->connect()->prepare('
//            SELECT * FROM public.Person WHERE email = :email
//        ');
//        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
//        $stmt->execute();
//
//        $user = $stmt->fetch(PDO::FETCH_ASSOC);
//
//        if ($user == false) {
//            return null;
//        }
//
//        return new User(
//            $user['Mass'],
//            $user['Height'],
//            $user['Gender'],
//            $user['Name'],
//            $user['Email'],
//            $user['Password'],
//        );
//    }
//}


require_once 'Repository.php';
require_once __DIR__ . '/../models/User.php';

class UserRepository extends Repository
{

    public function getUser(string $email): ?User
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.users WHERE email = :email
        ');
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->execute();

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($user == false) {
            return null;
        }

        return new User(
           $user['Mass'],
            $user['Height'],
            $user['Gender'],
            $user['email'],
            $user['Name'],
            $user['Password'],
        );
    }
}