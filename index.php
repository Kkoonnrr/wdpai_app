<?php

require 'Routing.php';

$path = trim($_SERVER['REQUEST_URI'], '/');
$path = parse_url($path, PHP_URL_PATH);

Routing::get('', 'DefaultController');
Routing::get('projects', 'DefaultController');
Routing::get('profile', 'DefaultController');
Routing::get('friends', 'DefaultController');
Routing::get('records', 'DefaultController');

Routing::post('login', 'SecurityController');
Routing::post('profile', 'SecurityController');

Routing::run($path);